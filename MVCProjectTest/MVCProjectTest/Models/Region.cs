﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCProjectTest.Models
{
    public class Region
    {
        [Key] // Buscar la forma de que no haga autoincremento pero que sea unico
        public int RegionId { get; set; }
        public int UniqueNumericCodeR { get; set; }
        public string RegionName { get; set; }
    }
}